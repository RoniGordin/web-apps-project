﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace app.Models
{
    public class Spa
    {
        public int Id { get; set; }

        [StringLength(30, MinimumLength = 1)]
        [Required]
        [Display(Name = "Name")]
        public string Name { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "Address")]
        public string Address { get; set; }

        [MinLength(2)]
        [MaxLength(200)]
        [Display(Name = "Description")]
        public string Description { get; set; }


        [Range(1, 100)]
        [Display(Name = "Max patients")]
        public int MaxPatients { get; set; }

        [Range(1, 1000)]
        [Display(Name = "Price per patient")]
        public int PricePerPatient { get; set; }

        [Range(1, 100)]
        [Display(Name = "Duration")]
        public int Duration { get; set; }

        [DataType(DataType.Url)]
        public string ImgUrl { get; set; }

        [Display(Name = "SpaType")]
        public SpaType SpaType { get; set; }
    }
}
    
