﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace app.Models
{
    public class Reservation
    {
        public int Id { get; set; }
        [Display(Name = "Patients")]
        public int Patients { get; set; }
        [Display(Name = "Total price")]
        public float TotalPrice { get; set; }
        [Display(Name = "Starting time")]
        public DateTime StartingTime { get; set; }

        [Display(Name = "Spa")]
        public Spa Spa { get; set; }

        [Display(Name = "User")]
        public ApplicationUser User { get; set; }
    }
}
