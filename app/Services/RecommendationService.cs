﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.IO;
using Microsoft.ML;
using Microsoft.ML.Data;
using Microsoft.ML.Trainers;
using Microsoft.ML.Runtime.Api;
using Microsoft.ML.Models;
using app.Models;
using Microsoft.ML.Transforms;

namespace app.Services
{
    public class RecommendationService
    {

        public PredictionModel<SpaData, SpaPrediction> Model { get; set; }
    
        public RecommendationService()
        {

        }

        /// <summary>
        /// Trains the model given a collection of reservations
        /// </summary>
        /// <param name="raw_data"></param>
        public void Train(ICollection<Reservation> raw_data, ICollection<SpaType> interests)
        {
            // Parses the data 
            var data = new List<SpaData>();

            foreach (var curr in raw_data)
            {
                float recommended = interests.Select(g => g.Id).Contains(curr.Spa.SpaType.Id) ? 1 : 0;
                data.Add(new SpaData()
                {
                    PricePerPatient = curr.Spa.PricePerPatient,
                    MaxPatients = curr.Spa.MaxPatients,
                    Duration = curr.Spa.Duration,
                    Recommended = recommended
                });
            }
            var collection = CollectionDataSource.Create(data);

            // Creates the learning pipe and add the parsed data
            var pipeline = new LearningPipeline();
            pipeline.Add(collection);
            pipeline.Add(new ColumnConcatenator("Features", "PricePerPatient", "MaxPatients", "Duration"));

            // Adds a classifier and train the model
            pipeline.Add(new LinearSvmBinaryClassifier()
            {
                Caching = CachingOptions.Memory,
                NormalizeFeatures = NormalizeOption.Auto
            });

            this.Model = pipeline.Train<SpaData, SpaPrediction>();
        }

        public IEnumerable<Spa> PredictRecommendedSpass(ICollection<Spa> spas)
        {
            List<Tuple<Spa, float>> spas_score = new List<Tuple<Spa, float>>();

            foreach (var spa in spas)
            {
                var prediction = this.Model.Predict(new SpaData()
                {
                    Duration = spa.Duration,
                    MaxPatients = spa.MaxPatients,
                    PricePerPatient = spa.PricePerPatient
                });

                spas_score.Add(new Tuple<Spa, float>(spa, prediction.PredictedRecommended));
            }

            return (spas_score.OrderBy(x => Math.Abs(x.Item2)).Select(y => y.Item1));
        }
    }

    public class SpaData
    {
        [ColumnName("Label")]
        [Column(ordinal: "0")]
        public float Recommended;
        [Column(ordinal: "1")]
        public float PricePerPatient;
        [Column(ordinal: "2")]
        public float MaxPatients;
        [Column(ordinal: "3")]
        public float Duration;
    }

    public class SpaPrediction
    {
        [ColumnName("Score")]
        public float PredictedRecommended;
    }
}
