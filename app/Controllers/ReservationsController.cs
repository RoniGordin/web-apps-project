﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using app.Data;
using app.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;

namespace app.Controllers
{
    public class ReservationsController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SpaContext _context;

        public ReservationsController(SpaContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        // GET: Reservations
        [Authorize]
        public async Task<IActionResult> Index(string spaName, int patients, string address)
        {
            ApplicationUser currentUser = await _userManager.GetUserAsync(HttpContext.User);

            var allReservations = _context.Reservations
                .Join(_context.Spas,
                reservation => reservation.Spa.Id,
                spa => spa.Id,
                (reservation, spa) => new { reservation, spa })
                .Join(_context.Users,
                combined => combined.reservation.User.Id,
                user => user.Id,
                (combined, user) => new Reservation()
                {
                    Id = combined.reservation.Id,
                    Spa = combined.spa,
                    Patients = combined.reservation.Patients,
                    StartingTime = combined.reservation.StartingTime,
                    TotalPrice = combined.reservation.TotalPrice,
                    User = user

                });

            if (!User.IsInRole("Admin"))
            {
                allReservations = allReservations.Where(r => r.User.Id == currentUser.Id);
            }

            if (!String.IsNullOrEmpty(spaName))
            {
                allReservations = allReservations.Where(r => r.Spa.Name.Contains(spaName));
            }
            if (!String.IsNullOrEmpty(address))
            {
                allReservations = allReservations.Where(r => r.Spa.Address.Contains(address));
            }
            if (patients > 0)
            {
                allReservations = allReservations.Where(r => (r.Patients == patients));
            }

            return View(await allReservations.ToListAsync());
        }

        // GET: Reservations/Details/5
        [Authorize]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new NotFoundViewResult("NotFoundError", "Reservation was not found");
            }

            var reservation = await _context.Reservations.Include(res => res.User)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (reservation == null)
            {
                return new NotFoundViewResult("NotFoundError", "Reservation was not found");
            }

            ApplicationUser user = await _userManager.GetUserAsync(HttpContext.User);
            // Checks if the user is system administrator
            if (User.IsInRole("Admin"))
            {
                return View(reservation);
            }
            // Checks if the user wants to view his own reservation
            else if (reservation.User.Id == user.Id)
            {
                return View(reservation);
            }
            else
            //return Json("access denied");
            {
                return new ForbiddenViewResult("PermissionDeniedError");
            }
        }

        // GET: Reservations/Create+
        public IActionResult Create(int? id)
        {
            if (id == null)
            {
                return null;
            }
            else
            {
                var spa = _context.Spas.Where(r => r.Id == id).Include(r => r.SpaType).FirstOrDefault();
                if (spa == null)
                {
                    return null;
                }
                else
                {
                    ViewBag.spa = spa;
                    return View();
                }
            }
        }

        // POST: Reservations/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<IActionResult> Create(int spaId, [Bind("Patients,TotalPrice,StartingTime")] Reservation reservation)
        {
            Spa spa = _context.Spas.Find(spaId);
            reservation.Spa = spa;
            ApplicationUser user = await _userManager.GetUserAsync(HttpContext.User);
            reservation.User = user;
            reservation.TotalPrice = spa.PricePerPatient * reservation.Patients;

            Reservation newRes = new Reservation()
            {
                Spa = reservation.Spa,
                TotalPrice = reservation.TotalPrice,
                User = user,
                StartingTime = reservation.StartingTime,
                Patients = reservation.Patients
            };

            if (ModelState.IsValid)
            {
                _context.Add(newRes);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Details), new { id = newRes.Id });
            }

            return RedirectToAction(nameof(Index));
        }

        // GET: Reservations/Edit/5
        [Authorize]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new NotFoundViewResult("NotFoundError", "Reservation was not found");
            }

            var reservation = await _context.Reservations.Include(r => r.Spa).Include(r => r.User).FirstOrDefaultAsync(r => r.Id == id);

            if (reservation == null)
            {
                return new NotFoundViewResult("NotFoundError", "Reservation was not found");
            }

            ApplicationUser user = await _userManager.GetUserAsync(HttpContext.User);
            if (reservation.User.Id == user.Id || User.IsInRole("Admin"))
            {
                ViewBag.spa = reservation.Spa;
                return View(reservation);
            }
            else
            {
                return new ForbiddenViewResult("PermissionDeniedError");
            }
        }

        // POST: Reservations/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<IActionResult> Edit(int id, int spaId, [Bind("Id,Patients,TotalPrice,StartingTime")] Reservation reservation)
        {
            if (id != reservation.Id)
            {
                return new NotFoundViewResult("NotFoundError", "Reservation was not found");
            }

            Spa spa = _context.Spas.Find(spaId);
            reservation.TotalPrice = spa.PricePerPatient * reservation.Patients;

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(reservation);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ReservationExists(reservation.Id))
                    {
                        return new NotFoundViewResult("NotFoundError", "Reservation was not found");
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(reservation);
        }

        // GET: Reservations/Delete/5
        [Authorize]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new NotFoundViewResult("NotFoundError", "Reservation was not found");
            }

            var reservation = await _context.Reservations.Include(r => r.User)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (reservation == null)
            {
                return new NotFoundViewResult("NotFoundError", "Reservation was not found");
            }

            ApplicationUser user = await _userManager.GetUserAsync(HttpContext.User);
            if (reservation.User.Id == user.Id || User.IsInRole("Admin"))
            {
                return View(reservation);
            }
            else
            {
                return new ForbiddenViewResult("PermissionDeniedError");
            }
        }

        // POST: Reservations/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var reservation = await _context.Reservations.FindAsync(id);
            _context.Reservations.Remove(reservation);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ReservationExists(int id)
        {
            return _context.Reservations.Any(e => e.Id == id);
        }
    }
}
