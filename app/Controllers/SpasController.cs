﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using app.Data;
using app.Models;
using internetiot.Handlers;
using Microsoft.AspNetCore.Authorization;
using app.Services;
using Microsoft.AspNetCore.Identity;

namespace app.Controllers
{
    public class SpasController : Controller
    {
        private readonly SpaContext _context;
        private readonly RecommendationService _rc;
        private readonly UserManager<ApplicationUser> _userManager;

        public SpasController(SpaContext context, RecommendationService rc, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _rc = rc;
            _userManager = userManager;
        }

        // GET: Spas
        public async Task<IActionResult> Index(string spaName, int patients, int maxPrice, string address)
        {
            var spas = _context.Spas.Include("SpaType");

            if (!String.IsNullOrEmpty(spaName))
            {
                spas = spas.Where(s => s.Name.Contains(spaName));
            }
            if (!String.IsNullOrEmpty(address))
            {
                spas = spas.Where(s => s.Address.Contains(address));
            }
            if (patients > 0)
            {
                spas = spas.Where(s => (s.MaxPatients >= patients));
            }
            if (maxPrice > 0)
            {
                spas = spas.Where(s => s.PricePerPatient <= maxPrice);
            }

            return View(await spas.ToListAsync());
        }

        // GET: Spas/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new NotFoundViewResult("NotFoundError", "Spa was not found");
            }

            var spa = await _context.Spas
                .FirstOrDefaultAsync(m => m.Id == id);
            if (spa == null)
            {
                return new NotFoundViewResult("NotFoundError", "Spa was not found");
            }

            return View(spa);
        }

        // GET: Spas/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Spas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Create([Bind("Id,Name,Address,Description,MaxPatients,PricePerPatient,Duration,ImgUrl, SpaType")] Spa spa)
        {
            string tname = ModelState.ToList().First(x => x.Key == "SpaType.Name").Value.RawValue.ToString();
            var type = _context.SpaType.Where(g => g.Name == tname);
            if (type.Count() != 0)
            {
                if (ModelState.IsValid)
                {
                    spa.SpaType = type.ToList()[0];
                    _context.Add(spa);
                    await _context.SaveChangesAsync();
                    FaceBookHandler.PostMessage(spa);
                    return RedirectToAction(nameof(Index));
                }

                return View(spa);
            }
            else
            {
                return new NotFoundViewResult("NotFoundError", "SpaType was not found");
            }

        }

        // GET: Spas/Edit/5
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new NotFoundViewResult("NotFoundError", "Spa was not found");
            }

            var spa = _context.Spas.Include(r => r.SpaType).Where(r => r.Id == id);
            if (spa.Count() == 0)
            {
                return new NotFoundViewResult("NotFoundError", "Room was not found");
            }
            return View(spa.ToList()[0]);
        }

        // POST: Spas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,Address,Description,MaxPatients,PricePerPatient,Duration,ImgUrl,SpaType")] Spa spa)
        {
            if (id != spa.Id)
            {
                return new NotFoundViewResult("NotFoundError", "Spa was not found");
            }

            if (ModelState.IsValid)
            {
                try
                {
                    string tname = ModelState.ToList().First(x => x.Key == "SpaType.Name").Value.RawValue.ToString();
                    var type = _context.SpaType.Where(g => g.Name == tname);
                    if (type.Count() != 0)
                    {
                        spa.SpaType = type.ToList()[0];
                        _context.Update(spa);
                        await _context.SaveChangesAsync();
                    }
                    else
                    {
                        return new NotFoundViewResult("NotFoundError", "SpaType was not found");
                    }

                }
                catch (DbUpdateConcurrencyException)
                {

                    return new NotFoundViewResult("NotFoundError", "Room was not found");
                }
                return RedirectToAction(nameof(Index));
            }
            return View(spa);
        }

        // GET: Spas/Delete/5
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var spa = await _context.Spas
                .FirstOrDefaultAsync(m => m.Id == id);
            if (spa == null)
            {
                return NotFound();
            }

            return View(spa);
        }

        // POST: Spas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            try
            {
                var spa = await _context.Spas.FindAsync(id);
                _context.Spas.Remove(spa);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            catch (DbUpdateException)
            {
                return new CanNotDeleteViewResult("CanNotDelete", "Can not delete this spa because there are reserved appointments attached to it.");
            }
        }

        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Statistics(string spaName, int patients, int maxPrice, string spaType)
        {
            var spaRooms = from r in _context.Spas
                           select r;

            if (!String.IsNullOrEmpty(spaName))
            {
                spaRooms = spaRooms.Where(s => s.Name.Contains(spaName));
            }

            if (patients > 0)
            {
                spaRooms = spaRooms.Where(s =>
                    (s.MaxPatients >= patients));
            }

            if (maxPrice > 0)
            {
                spaRooms = spaRooms.Where(s => s.PricePerPatient <= maxPrice);
            }

            if (!String.IsNullOrEmpty(spaType))
            {
                spaRooms = spaRooms.Where(s => s.SpaType.Name.Equals(spaType));
            }

            return View(await spaRooms.Include(r => r.SpaType).ToListAsync());
        }

        [Authorize(Roles = "Admin")]
        public JsonResult MostProfitableSpas()
        {
            var result = this._context.Reservations.GroupBy(x => x.Spa.Name).Select(s =>
            new
            {
                spaName = s.First().Spa.Name,
                Profit = s.Sum(p => p.TotalPrice)
            }).OrderByDescending(x => x.Profit).Take(5).ToList();
            
            return Json(result);
        }

        private bool SpaExists(int id)
        {
            return _context.Spas.Any(e => e.Id == id);
        }

        [HttpGet]
        [Authorize]
        public async Task<JsonResult> UserRecommendedSpas()
        {
            ApplicationUser user = await _userManager.GetUserAsync(HttpContext.User);
            var all_spas = _context.Spas.ToList();
            var all_reservations = _context.Reservations.Include(x => x.Spa.SpaType).ToList();
            var user_interests = _context.Reservations.Where(r => r.User.Id == user.Id).Select(x => x.Spa.SpaType).Distinct().ToList();
            this._rc.Train(all_reservations, user_interests);
            var recommended = this._rc.PredictRecommendedSpass(all_spas).Take(3).ToList();

            return Json(recommended.Select(x => x.Id));
        }
    }
}
