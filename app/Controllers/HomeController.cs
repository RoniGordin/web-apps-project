﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using app.Models;
using app.WebService;
using Microsoft.AspNetCore.Authorization;
using app.Data;

namespace app.Controllers
{
    public class HomeController : Controller
    {
        private SpaContext _context;
        public HomeController(SpaContext context)
        {
            this._context = context;
        }

        [AllowAnonymous]
        public IActionResult Index()
        {
            WeatherData telAviv = new WeatherData("Tel Aviv");
            telAviv.CheckWeather();
            ViewData["weather"] = telAviv.ToString();
            string weatherRecomand;
            if (telAviv.Temp <= 20)
            {
                weatherRecomand = "It's pretty cold outside, we advice you to stay in spa with jacuzzi.";
            }
            else if (telAviv.Temp > 16 && telAviv.Temp < 30)
            {
                weatherRecomand = "The weather is amazing! You can go to a spa with sea view.";
            }
            else
            {
                weatherRecomand = "It's very hot outside! Perfect weather for spa with a pool!";
            }
            ViewData["weather-recomendation"] = weatherRecomand;
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        public IActionResult Spas()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
