﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using app.Data;
using app.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace app.Controllers
{
    public class AdminController : Controller
    {
        private readonly SpaContext _context;
        private readonly UserManager<ApplicationUser> _userManager;

        public AdminController(SpaContext context, UserManager<ApplicationUser> userManager)
        {
            this._userManager = userManager;
            this._context = context;
        }

        public IActionResult Index()
        {
            return View();
        }

        [Authorize(Roles = "Admin")]
        public IActionResult MostProfitable()
        {
            return View();
        }

        [Authorize(Roles = "Admin")]
        public JsonResult RoomOrdersCountByMonth(int? id)
        {
            var result = this._context.Reservations.Where(r => r.Spa.Id == id).GroupBy(x => x.StartingTime.Month).Select(s =>
              new { Month = s.First().StartingTime.Month, Count = s.Count(), Profit = s.Sum(p => p.TotalPrice) }).ToList();

            return Json(result);
        }
    }
}
