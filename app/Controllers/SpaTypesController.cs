﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using app.Data;
using app.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;

namespace app.Controllers
{
    public class SpaTypesController : Controller
    {
        private readonly SpaContext _context;
        private readonly UserManager<ApplicationUser> _userManager;

        public SpaTypesController(SpaContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        // GET: SpaTypes
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Index(string searchString)
        {
            var spaTypes =  await _context.SpaType.ToListAsync();

            if (!String.IsNullOrEmpty(searchString))
            {
                spaTypes = spaTypes.FindAll(spaType => spaType.Name.Contains(searchString));
            }

            return View(spaTypes);
        }

        // GET: SpaTypes/Details/5
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var spaType = await _context.SpaType
                .FirstOrDefaultAsync(m => m.Id == id);
            if (spaType == null)
            {
                return NotFound();
            }

            return View(spaType);
        }

        // GET: SpaTypes/Create
        [Authorize(Roles = "Admin")]
        public IActionResult Create()
        {
            return View();
        }

        // POST: SpaTypes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Create([Bind("Id,Name,Description,MinAge")] SpaType spaType)
        {
            if (ModelState.IsValid)
            {
                _context.Add(spaType);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(spaType);
        }

        // GET: SpaTypes/Edit/5
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var spaType = await _context.SpaType.FindAsync(id);
            if (spaType == null)
            {
                return NotFound();
            }
            return View(spaType);
        }

        // POST: SpaTypes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,Description,MinAge")] SpaType spaType)
        {
            if (id != spaType.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(spaType);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!SpaTypeExists(spaType.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(spaType);
        }

        // GET: SpaTypes/Delete/5
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var spaType = await _context.SpaType
                .FirstOrDefaultAsync(m => m.Id == id);
            if (spaType == null)
            {
                return NotFound();
            }

            return View(spaType);
        }

        // POST: SpaTypes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            try
            {
                var spaType = await _context.SpaType.FindAsync(id);
                _context.SpaType.Remove(spaType);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            catch (DbUpdateException)
            {
                return new CanNotDeleteViewResult("CanNotDelete", "Can not delete this spa type because there are spas attached to it.");
            }
        }

        [Authorize(Roles = "Admin")]
        private bool SpaTypeExists(int id)
        {
            return _context.SpaType.Any(e => e.Id == id);
        }

        [Authorize(Roles = "Admin")]
        public IActionResult GetSpaTypesList()
        {
            var a = _context.SpaType.Select(x => x.Name).ToList();
            return Json(_context.SpaType.Select(x => x.Name).ToList());
        }

        [Authorize(Roles = "Admin")]
        public JsonResult GetSpasPerSpaTypeGraph()
        {

            return Json((from t in _context.SpaType
                         join s in _context.Spas
                         on t.Id equals s.SpaType.Id
                         select new
                         {
                             SpaType = t.Name,
                             SpaName = s.Name

                         })
                     .GroupBy(x => x.SpaType)
                     .Select(t => new { SpaType = t.Key, NumOfSpas = t.Count() }));
        }

        [Authorize(Roles = "Admin")]
        public IActionResult GetSpaTypeList()
        {
            return Json(_context.SpaType.Select(g => g.Name).ToList());
        }
    }
}
