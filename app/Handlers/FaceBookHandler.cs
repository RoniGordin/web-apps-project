﻿using System;
using System.Dynamic;
using System.IO;
using System.Text;
using System.Net;
using Newtonsoft.Json;
using Facebook;
using app.Models;

namespace internetiot.Handlers
{
    public class FaceBookHandler
    {
        private const string FacebookApiID = "1357660257772391";
        private const string FacebookApiSecret = "4c40fad79531635f7c2f19f037448ca2";

        private const string PageID = "130088968534973";                                      
        private const string fb_exchange_token = "EAATSyOlj42cBAOtyS948d17wyP8xiCImxawlEMBxqC55lWLfXSSd3E0zh45n9haPwJqo2IV19nGqqGLjZBP4iU2wx6G4jw4uayHg9se1xUJeFits0sdmSX8ZAvXN3kXgOafgZCEv5xWXPvaAzg4BfUgAC8LemCe2qTIunAKl3cQUDFoQIBl";

        private const string AuthenticationUrlFormat =
            "https://graph.facebook.com/oauth/access_token?grant_type=fb_exchange_token&client_id={0}&client_secret={1}&fb_exchange_token={2}";


        static string GetAccessToken(string apiID, string apiSecret, string pageID)
        {
            string accessToken = string.Empty;
            string url = string.Format(AuthenticationUrlFormat, apiID, apiSecret, fb_exchange_token);

            WebRequest request = WebRequest.Create(url);
            WebResponse response = request.GetResponse();

            using (System.IO.Stream responseStream = response.GetResponseStream())
            {
                StreamReader reader = new StreamReader(responseStream, Encoding.Default);
                String responseString = reader.ReadToEnd();

                dynamic stuff = JsonConvert.DeserializeObject(responseString);

                accessToken = stuff["access_token"];
            }

            if (accessToken.Trim().Length == 0)
                throw new Exception("There is no Access Token");

            return accessToken;
        }

        public static void PostMessage(Spa spa)
        {
            try
            {
                string accessToken = fb_exchange_token;
                FacebookClient facebookClient = new FacebookClient(accessToken);

                dynamic messagePost = new ExpandoObject();
                messagePost.access_token = accessToken;
                messagePost.message = "Check Out The New Spa: " + spa.Name + "\n\n Now Only In: " + spa.PricePerPatient + "₪ for participant.";

                string url = string.Format("/{0}/feed", PageID);
                var result = facebookClient.Post(url, messagePost);
            }
            catch(Exception error)
            {
                Console.WriteLine(error);
            }
        }
    }
}